/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( !window.requestAnimationFrame ) {
  window.requestAnimationFrame = ( function() {
    return window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function( /* function FrameRequestCallback */ callback, /* DOMElement Element */ element ) {
      window.setTimeout( callback, 1000 / 60 );
    };
  })();
}



var Game = {};


Game.init = function(){
    

    Game.state = 0 ;
    
    
    var WIDTH = 1280,
	    HEIGHT = 800;

	// set some camera attributes
    var VIEW_ANGLE = 45,
        ASPECT = WIDTH / HEIGHT,
        NEAR = 0.1,
        FAR = 10000;





    Game.renderer = new THREE.WebGLRenderer();
     
     Game.renderer.setSize(WIDTH, HEIGHT);
     document.body.appendChild(Game.renderer.domElement);    
     
     
   
     
     Game.stats = new Stats();
  Game.stats.domElement.style.position = 'absolute';
  Game.stats.domElement.style.top = '10px';
  Game.stats.domElement.style.left = '10px';
  document.body.appendChild( Game.stats.domElement );

     Game.gameOver = false;
     Game.start();     
};
     
     
     Game.start = function(){
         
         World.init();

         Game.animate();

     };
     
     
     Game.animate = function(){
         
         Game.stats.update();
        
        
        if(Game.state == 0){
            World.animate();
        }
        
        if(Game.state == 1){
            Battle.animate();
            
        }
        
        
        
         
         
         if(!Game.gameOver) {window.requestAnimationFrame(Game.animate);}
         


         
         
     };
     
     
    
