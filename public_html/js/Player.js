/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Player = function(){


    this.HP = 100;
    
    this.locationFieldX = 1;
    this.locationFieldY = 1;
 
    this.location3DX = -80;
    this.location3DY = 0;
    this.location3DZ = 20;
 
    this.playerModel;
    this.playerMaterial;
    
    this.createModel();

};


    Player.prototype.moveTo = function(where){
        
                     text = document.getElementById("container");

             text.innerHTML = where; 
        if(where=="Up"){
            if(Battle.getCanMoveTo(this.locationX,this.locationY,this.locationX,this.locationY+1,0)==true){
                this.locationY=this.locationY+1;
            }
        };
        if(where=="Down"){
            if(Battle.getCanMoveTo(this.locationX,this.locationY,this.locationX,this.locationY-1,0)==true){
                this.locationY=this.locationY-1;
            }
        };
        if(where=="Left"){
            if(Battle.getCanMoveTo(this.locationX,this.locationY,this.locationX-1,this.locationY,0)==true){
                this.locationX=this.locationX-1;
            }
        };
        if(where=="Right"){
            if(Battle.getCanMoveTo(this.locationX,this.locationY,this.locationX+1,this.locationY,0)==true){
                this.locationX=this.locationX+1;
            }
        };
    };
    
    Player.prototype.getX = function(){
        return this.locationFieldX;};

    Player.prototype.getY = function(){
        return this.locationFieldY;};
    
    Player.prototype.getXY = function(){
        return [this.locationFieldX,this.locationFieldY];};
    
    
 Player.prototype.handleIt = function(){
     
             activeKeysPlayer = KeyboardJS.activeKeys();
             
             text = document.getElementById("container");

             text.innerHTML = this.getXY(); 
             
             
             for( i in KeyboardJS.activeKeys()){
                 
                
                 if(KeyboardJS.activeKeys()[i] == "s"){
                         this.locationFieldX -= 1;
                         this.limitX();
                         break;
             break;
                 }
                 if(KeyboardJS.activeKeys()[i] == "w"){
                      this.locationFieldX += 1;    
                      this.limitX();
                      break;
                 }    
                if(KeyboardJS.activeKeys()[i] == "a"){
                     this.locationFieldY+= 1;
                     this.limitY();
                     break;
                 }  
                if(KeyboardJS.activeKeys()[i] == "d"){
                     this.locationFieldY -= 1;
                     this.limitY();
                     break;
                 }  

             };
             
             this.calculate3DLocation();
             this.playerModel.position.set(this.location3DX,this.location3DY,this.location3DZ);
         
 
 };


 Player.prototype.limitX = function(){
     
     if(this.locationFieldX<0){this.locationFieldX=0;};
     if(this.locationFieldX>2){this.locationFieldX=2;};

     
 };
 
 
 Player.prototype.limitY = function(){
     if(this.locationFieldY<0){this.locationFieldY=0;};
     if(this.locationFieldY>2){this.locationFieldY=2;};
     
 };
 
 
 Player.prototype.createModel = function(){
     
     	this.playerMaterial =
	  new THREE.MeshLambertMaterial(
		{
		  color: 0x0000FF
		});
     
     
     this.playerModel = new THREE.Mesh(

	  new THREE.CubeGeometry(
		20,
		40,
		20,
		1,
		1,
		1),

	  this.playerMaterial);

 };
 
 Player.prototype.calculate3DLocation = function(){
     
     this.location3DX = -132 + (66 * this.locationFieldX);
     this.location3DY = -66 + (66 * this.locationFieldY);
     
     
     	Battle.camera.position.x = this.location3DX - 100;
	Battle.camera.position.y = (this.location3DY)
	Battle.camera.position.z = this.location3DZ + 100 ;
	
	// rotate to face towards the opponent
	Battle.camera.rotation.x = Math.PI/180;
	Battle.camera.rotation.y = -60 * Math.PI/180;
	Battle.camera.rotation.z = -90 * Math.PI/180;
        
       //Battle.camera.lookAt(this.playerModel.position);

    
     
     
     
     
     
     
     
 };