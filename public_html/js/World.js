/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

World = {};

var imagePrefix = "textures/";
var directions = ["front", "back", "up", "down", "left", "right"];
var imageSuffix = ".png";





World.init = function(){
    
    
    
    World.moveZ = 0.0;
    World.moveX = 0.0;
    
        
    
    var WIDTH = 1280,
	    HEIGHT = 800;

	// set some camera attributes
    var VIEW_ANGLE = 90,
        ASPECT = WIDTH / HEIGHT,
        NEAR = 0.1,
        FAR = 10000;
    
    

 World.scene = new THREE.Scene();
      World.camera = new THREE.PerspectiveCamera(  VIEW_ANGLE,
	                                ASPECT,
	                                NEAR,
	                                FAR  );
    
     World.scene.add(World.camera);
     

     
     World.geometry = new THREE.SphereGeometry( 5, 50, 50 );

     World.light = new THREE.PointLight(0xffffff);
     World.light.position.set(0,1500,500);
     World.scene.add(World.light);
     
     World.texture = THREE.ImageUtils.loadTexture( 'textures/earthmap.jpg' );
     World.material = new THREE.MeshLambertMaterial( { map: World.texture } );
     
     
     World.loader = new THREE.JSONLoader();
     World.loader.load( 'terrain.json', function ( geometry ) {
		        	//var material = new THREE.MeshLambertMaterial({
					  //  map: THREE.ImageUtils.loadTexture('./textures/terrain.png'),  // specify and load the texture
					  //});
		        	//var tekstura = THREE.ImageUtils.loadTexture( 'textures/terrain.png' );
		        	//var materijal = new THREE.MeshPhongMaterial( map: tekstura );
    World.mesh = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( {map: THREE.ImageUtils.loadTexture( 'textures/terrain.png' )} ));
    World.mesh.scale.x = 400;
    World.mesh.scale.y = 400;
    World.mesh.scale.z = 400;
    World.mesh.geometry.computeBoundingBox();
   World.terrainBoundingBox =  World.mesh.geometry.boundingBox.clone();
    
    
    World.scene.add( World.mesh );

	}); 


  World.skyGeometry = new THREE.CubeGeometry( 4000, 4000, 4000 );
    //  World.skyGeometry = new THREE.CubeGeometry( World.terrainBoundingBox.min.x+World.terrainBoundingBox.max.x, 4000, 4000 );

    
    World.materialArray = [];
    for (var i = 0; i < 6; i++)
				World.materialArray.push( new THREE.MeshBasicMaterial({
					map: THREE.ImageUtils.loadTexture( imagePrefix + directions[i] + imageSuffix ),
					side: THREE.BackSide
				}));
    World.skyMaterial = new THREE.MeshFaceMaterial( World.materialArray );
    World.skyBox = new THREE.Mesh( World.skyGeometry, World.skyMaterial );
    World.scene.add( World.skyBox );

     
     World.stepTime = 100;
     World.frameTime = 0; // ms
     World.cumulatedFrameTime = 0; // ms
     World._lastFrameTime = Date.now(); // timestamp
     
  
     
           World.distance = 0;
           
           
           World.jumping = false;
           World.falling = false;
           World.jumpOriginalYPosition = 0;
           
           World.jumpTime = 0;
           
           World.clock = new THREE.Clock();
           
           World.character;
           
      World.characterLoader = new THREE.JSONLoader();
            World.characterLoader.onLoadComplete = function () {
              World.scene.add( World.character );
            }
            World.characterLoader.load( 'character.json', function (geometry) {
              World.character = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( {map: THREE.ImageUtils.loadTexture( 'textures/characterGreen.png' )} ));
              World.character.position.set(20,0,0);
              World.character.scale.set(3,3,3);
              World.baseY = World.character.position.y; 
            });

            //red character
            World.characterRedLoader = new THREE.JSONLoader();
            World.characterRedLoader.onLoadComplete = function () {
              World.scene.add( World.characterRed );
            }
            World.characterRedLoader.load( 'character.json', function (geometry) {
              World.characterRed = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( {map: THREE.ImageUtils.loadTexture( 'textures/characterRed.png' )} ));
              World.characterRed.position.set(100,0,-1000);
              World.characterRed.scale.set(3,3,3);
            });

            //blue character
            World.characterBlueLoader = new THREE.JSONLoader();
            World.characterBlueLoader.onLoadComplete = function () {
              World.scene.add( World.characterBlue );
            }
            World.characterBlueLoader.load( 'character.json', function (geometry) {
              World.characterBlue = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( {map: THREE.ImageUtils.loadTexture( 'textures/characterBlue.png' )} ));
              World.characterBlue.position.set(-1300,0,-550);
              World.characterBlue.scale.set(3,3,3);
              World.characterBlue.rotation.y += Math.PI / 2;
            });
            
            World.groundVector = new THREE.Vector3(0,-1,0);
            Game.renderer.shadowMapEnabled = true;
            

   
   };

     
     
    World.animate = function(){
        
        
       // World.frontVector = new THREE.Vector3(World.camera.getWorldDirection().x,0,World.camera.getWorldDirection().z).normalize();
        
        
        
        
        text = document.getElementById("container");
        
      if(!World.character && !World.characterRed && !World.characterBlue){
          
          
         var time = Date.now();
         
         World.frameTime = time - World._lastFrameTime;
         World._lastFrameTime = time;
         World.cumulatedFrameTime += World.frameTime;
         
                                
         while(World.cumulatedFrameTime > World.stepTime){
            
            text.innerHTML = "Time since start" + World.clock.getElapsedTime()+"\n"
            +"Loaded world: \n" + World.mesh +"\n"
            +"Loaded Player Character: \n" + World.character +"\n"
            +"Loaded Red: \n" + World.characterRed +"\n"
            +"Loaded Blue: \n" + World.characterBlue+"\n";
    
            World.cumulatedFrameTime -= World.stepTime;
      }}
                     
        if (World.character && World.characterRed && World.characterBlue){
            
           text.innerHTML = "";
        World.ray = new THREE.Raycaster(new THREE.Vector3(World.character.position.x,World.character.position.y+500 ,World.character.position.z),World.groundVector.clone());

       // World.ray2 = new THREE.Raycaster(new THREE.Vector3(World.character.position.x,World.character.position.y ,World.character.position.z),World.frontVector.clone());

        
        
        World.intersects = World.ray.intersectObject(World.mesh);
     //   World.moveCollision = World.ray.intersectObject(World.mesh);
        

        World.distance=0;

        if ( World.intersects.length > 0 )
	{
            
            INTERSECTED = World.intersects[ 0 ];
            World.distance = INTERSECTED.point.y;
              //INTERSECTED.object.material.color.setHex(0x00D66B);
              
              text.innerHTML = World.jumpTime - World.clock.getElapsedTime() ; //BUG: Zbelja če ni tega
         //     text.innerHTML = World.frontVector.z+" "+ World.frontVector.x + " " +World.frontVector.y;
               text.innerHTML = "";
        }
        
    //    if ( World.moveCollision.length > 0 ){
     //       
       //     text.innerHTML = World.moveCollision[0].point.x;

       // }


          var battle = false;

          //distance to red
          var dxR = World.character.position.x - World.characterRed.position.x;
          var dyR = World.character.position.y - World.characterRed.position.y;
          var dzR = World.character.position.z - World.characterRed.position.z;
          var razdaljaRed = Math.sqrt(dxR*dxR+dyR*dyR+dzR*dzR);

          //distance to blue
          var dxB = World.character.position.x - World.characterBlue.position.x;
          var dyB = World.character.position.y - World.characterBlue.position.y;
          var dzB = World.character.position.z - World.characterBlue.position.z;
          var razdaljaBlue = Math.sqrt(dxB*dxB+dyB*dyB+dzB*dzB);

          if (razdaljaRed < 100 || razdaljaBlue < 100){
            battle = true;
          text.innerHTML = battle;
          }
          else
            text.innerHTML = battle;
        text.innerHTML = "";


            
     
         var time = Date.now();
         
         World.frameTime = time - World._lastFrameTime;
         World._lastFrameTime = time;
         World.cumulatedFrameTime += World.frameTime;
         
                                
         while(World.cumulatedFrameTime > World.stepTime){
             
          if(World.jumping == true){
              if(World.jumpTime - World.clock.getElapsedTime() > -2){
                  World.character.position.y += 10*(World.clock.getElapsedTime()/World.jumpTime);
              }
              else{World.jumping = false;World.falling=true;}   
          };            
          
          if(World.character.position.y > World.distance+World.baseY && World.jumping == false){
              World.character.position.y -= 10;
          }     
          else{World.falling = false;}

             for( i in KeyboardJS.activeKeys()){
                 
                 
                 if(KeyboardJS.activeKeys()[i] == "a"){
                     World.character.rotateOnAxis( new THREE.Vector3(0,1,0), 0.1*(Math.PI/2));
                     
                 };
                 if(KeyboardJS.activeKeys()[i] == "d"){
                     World.character.rotateOnAxis( new THREE.Vector3(0,1,0), -0.1*(Math.PI/2));
                     
                 } ;   
                if(KeyboardJS.activeKeys()[i] == "w"){
                  World.character.translateZ(20);
                    if(World.jumping==false && World.falling==false){ //&& World.MovingCube.position.y-(World.baseY + Math.round(World.distance))<10
                    World.character.position.y = World.baseY + Math.round(World.distance);
                }
                 }  ;
                if(KeyboardJS.activeKeys()[i] == "s"){
                    //World.moveZ += 2.5;
                  World.character.translateZ(-20);
                   if(World.jumping==false && World.falling==false){
                     World.character.position.y = World.baseY + Math.round(World.distance);
                   }
                 }  ;
                 
                if(KeyboardJS.activeKeys()[i] == "b"){
                 if (battle){    
                    Battle.init();
                    Game.state = 1;
                }
                     break;
                 }  ;
                 
                if(KeyboardJS.activeKeys()[i] == "spacebar" && World.jumping == false && World.falling == false){
                    World.jumpTime =  World.clock.getElapsedTime();
                    World.jumping = true;
                    World.jumpOriginalYPosition = World.character.position.y;
                 }  ;   
             };
  
             
        World.cumulatedFrameTime -= World.stepTime;
             
        World.relativeCameraOffset = new THREE.Vector3(0,50,-50); 
        World.cameraOffset = World.relativeCameraOffset.applyMatrix4( World.character.matrixWorld );
        
        World.camera.position.x = World.cameraOffset.x;
	World.camera.position.y = World.cameraOffset.y;
	World.camera.position.z = World.cameraOffset.z;
        World.camera.lookAt(World.character.position);
         
             
             
        Game.renderer.render(World.scene,World.camera);

             
            
         }
             
             
         }};